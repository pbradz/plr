source("test-induced-corr.R")
source("plm-power-functions.R") 

phy.names <- names(phy.clusters)
names(phy.names) <- phy.names
uncensored.results <- lapply(phy.names, function(phylum) {
  message(phylum)
  message(Sys.time())
  hits <- overall.sigs[[phylum]][["strong"]]
  non.hits <- setdiff(names(overall.results[[phylum]][1, ] %>% na.omit),
                      hits)
  equal.non.hits <- sample(non.hits, length(hits), replace = FALSE)
  hits.and.non.hits <- c(hits, equal.non.hits)
  matrix.censored.plm(chronos.overall.trees[[phylum]],
    phylum.ff.bin.restricted[[phylum]][hits.and.non.hits, ],
    species.prev.avg.logit,
    nsubsamples = 2000,
    restrict = phy.clusters[[phylum]] %>% as.character,
    cores = 12)
})

dynamic.pheno.cutoff <- fake.uncensor(species.prev.avg.logit,
                                      return.fit = TRUE)$at.which
print(dynamic.pheno.cutoff)

power.censor.results <- lapply(list(fx0.00 = 0, fx0.75 = 0.75), function(fx) {
  lapply(chronos.overall.trees, function(phy) {
    sigma2 <- phylolm(phy = phy, species.prev.avg.logit[intersect(names(species.prev.avg.logit), phy$tip.label)] ~ 1)$sigma2
    params = list(alpha = 0, sigma2 = sigma2, ancestral.state = -4.67, optimal.value = 0)
    lapply(1:3, function(.) { # 3 replicates
      force(dynamic.pheno.cutoff)
      ou1.plm.distro <- test.power(phy, effect.size = fx, distro = TRUE, B = 250, contParams = params, dynamic.truncate = dynamic.pheno.cutoff, n.cores = 12)
      ou1.cplm.distro <- test.power(phy, effect.size = fx, distro = TRUE, B = 250, contParams = params, dynamic.truncate = dynamic.pheno.cutoff, type = "phylo.censored", n.cores = 12)
      ou1.eplm.distro <- test.power(phy, effect.size = fx, distro = TRUE, B = 250, contParams = params, dynamic.truncate = dynamic.pheno.cutoff, type = "phylo.empirical", n.cores = 12)
      return(list(plm = ou1.plm.distro, cplm = ou1.cplm.distro, eplm = ou1.eplm.distro))
    })
  })
})
saveRDS(file="power-censor-comparison.rds", power.censor.results)

use.cluster  <- makeCluster(12, outfile="")
power.censor.big.results <- lapply(list(fx0.00 = 0, fx0.75 = 0.75), function(fx) {
  lapply(list(alpha0 = 0, alpha25 = 25, alpha50 = 50), function(alpha) {
    lapply(list(censorn1.0 = -1.0, censorn0.5 = -0.5, censorn0 = 0, censorp0.5 = 0.5), function(trunc) {
      lapply(chronos.overall.trees, function(phy) {
        plm.fit <- phylolm(phy = phy, species.prev.avg.logit[intersect(names(species.prev.avg.logit), phy$tip.label)] ~ 1)
        sigma2 <- plm.fit$sigma2
        ancestral <- plm.fit$coefficients[1]
        params = list(alpha = alpha, sigma2 = sigma2, ancestral.state = ancestral, optimal.value = 0)
        lapply(1:3, function(.) { # 3 replicates
          force(dynamic.pheno.cutoff)
          ou1.plm.distro <- test.power(phy, effect.size = fx, distro = TRUE, B = 250, contParams = params, dynamic.truncate = trunc, n.cores = 12, cl = use.cluster)
          ou1.cplm.distro <- test.power(phy, effect.size = fx, distro = TRUE, B = 250, contParams = params, dynamic.truncate = trunc, type = "phylo.censored", n.cores = 12, cl = use.cluster)
          ou1.eplm.distro <- test.power(phy, effect.size = fx, distro = TRUE, B = 250, contParams = params, dynamic.truncate = trunc, type = "phylo.empirical", n.cores = 12, cl = use.cluster)
          return(list(plm = ou1.plm.distro, mock.unc.plm = ou1.cplm.distro, para.boot.plm = ou1.eplm.distro))
        })
      })
    })
  })
})
saveRDS(file="power-censor-big-comparison.rds", power.censor.big.results)
stopCluster(use.cluster)
censor.result.table <- annotate.nested(power.censor.results, n.names = c("fx", "phylum", "replicate", "model"), summarize = function(x) mean(x <= 0.05, na.rm = TRUE))[,-1]
ggplot(censor.result.table, aes(phylum, y = value, fill = phylum, group = replicate)) + geom <- bar(aes(group=replicate, y=value), stat="identity", position=position <- dodge()) + facet <- grid(fx ~ model) + scale <- fill <- manual(values=as.character(chart.cols)) + geom <- hline(yintercept=0.05, lty = 2) + theme(axis.text.x = element <- text(angle = 90, hjust = 1))


power.censor.short.results.2 <- lapply(list(fx0.00 = 0, fx0.75 = 0.75), function(fx) {
  phy <- chronos.overall.trees$Bacteroidetes
    sigma2 <- phylolm(phy = phy, species.prev.avg.logit[intersect(names(species.prev.avg.logit), phy$tip.label)] ~ 1)$sigma2
    params = list(alpha = 0, sigma2 = sigma2, ancestral.state = -4.67, optimal.value = 0)
      force(dynamic.pheno.cutoff)
      ou1.plm.distro <- test.power(phy, effect.size = fx, distro = TRUE, B = 250, contParams = params, dynamic.truncate = dynamic.pheno.cutoff, n.cores = 12)
      ou1.cplm.distro <- test.power(phy, effect.size = fx, distro = TRUE, B = 250, contParams = params, dynamic.truncate = dynamic.pheno.cutoff, type = "phylo.censored", n.cores = 12)
      ou1.eplm.distro <- test.power(phy, effect.size = fx, distro = TRUE, B = 250, contParams = params, dynamic.truncate = dynamic.pheno.cutoff, type = "phylo.empirical", n.cores = 12)
      return(list(plm = ou1.plm.distro, cplm = ou1.cplm.distro, eplm = ou1.eplm.distro))
  
})
saveRDS(file="power-censor-short-comparison.rds", power.censor.short.results)
censor.short.result.table <- annotate.nested(power.censor.short.results, n.names = c("fx", "model"), summarize = function(x) mean(x <= 0.05, na.rm = TRUE))[,-1]
censor.short.result.table.2 <- annotate.nested(power.censor.short.results.2, n.names = c("fx", "model"), summarize = function(x) mean(x <= 0.05, na.rm = TRUE))[,-1]


power.bact.empirical.results <- lapply(list(fx0.00 = 0, fx0.75 = 0.75), function(fx) {
  phy <- chronos.overall.trees[["Bacteroidetes"]]
  sigma2 <- phylolm(phy = phy, species.prev.avg.logit[intersect(names(species.prev.avg.logit), phy$tip.label)] ~ 1)$sigma2
  params = list(alpha = 0, sigma2 = sigma2, ancestral.state = -4.67, optimal.value = 0)
  lapply(1:4, function(.) { # 4 replicates
    force(dynamic.pheno.cutoff)
    ou1.eplm.distro <- test.power(phy, effect.size = fx, distro = TRUE, B = 500, contParams = params, dynamic.truncate = dynamic.pheno.cutoff, type = "phylo.empirical", n.cores = 12)
    return(ou1.eplm.distro)
  })
})

empirical.result.table <- annotate.nested(power.bact.empirical.results, n.names = c("fx", "replicate"), summarize = function(x) mean(x <= 0.05, na.rm = TRUE))[,-1]
empirical.result.table$phylum <- "Bacteroidetes"
empirical.result.table$model <- "eplm"
all.result.table <- rbind(empirical.result.table[, colnames(censor.result.table)], censor.result.table)
bact.results <- all.result.table[all.result.table$phylum == "Bacteroidetes", ]
