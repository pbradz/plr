#!/usr/bin/python

# generate power-list.txt for sge

import os, sys, argparse, itertools

# iterate over fx, alpha, k, phy for given B, add outfile

parser = argparse.ArgumentParser()
parser.add_argument("--fx", nargs = "*", type = float, default = [0.0, 0.75])
parser.add_argument("--alpha", nargs = "*", type = float, default = [0, 25, 50])
parser.add_argument("--trunc", nargs = "*", type = float,
    default = [-1, -0.5, 0, 0.5])
parser.add_argument("--reps", type = int, default = 3)
parser.add_argument("--phy", nargs = "*", type = str,
    default = ["Bacteroidetes",
               "Firmicutes",
               "Actinobacteria",
               "Proteobacteria"])
parser.add_argument("--nB", type = int, default = 250)
parser.add_argument("--outbase", type = str, default = "outfile")
args = parser.parse_args()

iterover = itertools.product(args.fx,
    args.alpha,
    args.trunc,
    args.phy)

for i, (f, a, t, p) in enumerate(itertools.product(args.fx, args.alpha,
  args.trunc, args.phy)):
  print "%f %f %f %d %d %s %s-%05d.rds" % (f, a, t, args.reps, args.nB, p, args.outbase, i)




