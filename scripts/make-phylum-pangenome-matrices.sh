#!/bin/bash

for x in bacteroidetes actinobacteria firmicutes proteobacteria; do 
	./make-figfam-matrix-pangenome.py --clusters ../phy-${x}.txt -o ./genome-pangenome-output/$x-figfams.txt
done
