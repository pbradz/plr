#!/usr/bin/python

import sys, os, gzip, csv

import argparse

parser = argparse.ArgumentParser(description = "Make table of FIGfam \
    presence/absence.")
parser.add_argument('--clusters', '-c', dest = 'clusterfile', type = str,
    help = 'File containing genome cluster list',
    default = "../phy-bacteroidetes.txt")
parser.add_argument('--directory', '-d', dest = 'dir', type = str,
    help = 'Directory where we expect to find genome clusters',
    default = "../../data/midas/midas_db_v1.0/genome_clusters")
parser.add_argument('--startline', '-s', dest = 'sl', type = int,
    help = 'What line to start at (0 is first)',
    default = 0)
parser.add_argument('--endline', '-e', dest = 'el', type = int, 
    help = 'What line to end at (default is EOF)',
    default = None)
parser.add_argument('--output', '-o', dest = 'of', type = str, 
    help = 'Output file (none defaults to sys.stdout())',
    default = None)
parser.add_argument('--no-messages', '-n', dest = 'messages',
    action = 'store_false',
    help = 'Suppress messages')
parser.set_defaults(messages=True)
args = parser.parse_args()

GC_DIR = args.dir
RESTRICT = args.clusterfile

with open(RESTRICT, 'rb') as fh:
  restricted = [line[:-1] for line in fh]

# custom start/end lines
if args.el is None:
  restricted = restricted[args.sl:]
else:
  if (args.el <= len(restricted)):
    restricted = restricted[args.sl:args.el]
  else:
    restricted = restricted[args.sl:]

(dirname, dirnames, fnames) = os.walk(GC_DIR).next()
found_restricted = [d for d in dirnames if d in restricted]

if args.of is not None:
  ofh = open(args.of, 'w')
else:
  ofh = sys.stdout

if args.messages:
  sys.stderr.write("Reading in genome and pangenome files for %s...\n" %
      args.clusterfile)

fams = dict()
allf = list()
# allfams = set()
for d in found_restricted:  # testing with first ten dirs
  fams[d] = dict()
  if args.messages:
    sys.stderr.write("%s... " % d)
  cluster_dir = os.path.join(GC_DIR, d)
  function_file_name = os.path.join(cluster_dir, "pangenome.functions.gz")
  if not os.path.isfile(function_file_name):
    sys.stderr.write("pangenome feature file not found for %s" % d)
    continue
  with gzip.open(function_file_name, mode='rb') as fh:
    reader = csv.reader(fh, delimiter = "\t")
    header = reader.next()
    try:
      function_col = header.index("function_id")
      ontology_col = header.index("ontology")
    except:
      if args.messages:
        sys.stderr.write("genome features file does not appear to contain functions/ontologies, skipping %s" % d)
      continue
    for row in reader:
      if row[ontology_col] == "figfam":
        ff = row[function_col]
        # allfams.add(ff)
        allf.append(ff)
        if ff in fams[d].keys():
          fams[d][ff] += 1
        else:
          fams[d][ff] = 1
      else:
        continue
  with gzip.open(os.path.join(cluster_dir, "genome.features.gz"), mode='rb') as fh:
    reader = csv.reader(fh, delimiter = "\t")
    header = reader.next()
    try:
      figfam_col = header.index("figfam_ids")
    except:
      if args.messages:
        sys.stderr.write("genome features file does not appear to contain figfam_ids, skipping %s" % d)
      continue
    for row in reader:
      ff = row[figfam_col] 
      if (ff == ""): continue
      allf.append(ff)
      if ff in fams[d].keys():
        fams[d][ff] += 1
      else:
        fams[d][ff] = 1

if args.messages:
  sys.stderr.write("\n\nOrganizing clusters...\n")

allfams = set(allf)
clusters = list(set(fams.keys()))
for c in clusters:
  for f in allfams:
    if not fams[c].has_key(f):
      fams[c][f] = 0

if args.messages:
  sys.stderr.write("Writing to disk...\n")
out_header = ['family'] + clusters
writer = csv.DictWriter(ofh, fieldnames = out_header, extrasaction="ignore")
writer.writeheader()
for f in allfams:
  output_dict = dict()
  for c in clusters:
    output_dict[c] = fams[c][f]
  output_dict['family'] = f
  writer.writerow(output_dict)

ofh.close()
