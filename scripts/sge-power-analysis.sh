#!/bin/bash                         #-- what is the language of this shell
#                                  #-- Any line that starts with #$ is an instruction to SGE
#$ -S /bin/bash                     #-- the shell for the job
#$ -o /pollard/shattuck0/pbradz/plr-power-analysis/o                        #-- output directory (fill in)
#$ -e /pollard/shattuck0/pbradz/plr-power-analysis/e                        #-- error directory (fill in)
#$ -cwd                            #-- tell the job that it should start in your working directory
#$ -j y                            #-- tell the system that the STDERR and STDOUT should be joined
#$ -l mem_free=2G                  #-- submits on nodes with enough free memory (required)
#$ -l arch=linux-x64               #-- SGE resources (CPU type)
#$ -l netapp=1G,scratch=1G         #-- SGE resources (home and scratch disks)
#$ -l h_rt=48:00:00                #-- runtime limit (see above; this requests 24 hours)
#$ -t 1-9                       #-- remove first '#' to specify the number of
                                   #-- tasks if desired (see Tips section)

date
hostname

cd $HOME/src/plr/
POWER_LIST=$HOME/src/plr/scripts/power-list-remaining.txt
POWER_ARGS=`sed -n ${SGE_TASK_ID}p $POWER_LIST`
Rscript sge-cluster-power-analysis.R $POWER_ARGS

qstat -j $JOB_ID                                  # This is useful for debugging and usage purposes,
                                                  # e.g. "did my job exceed its memory request?"

