library("mvMORPH")

make.random.selective.regimes <- function(test.phy, n, min.num = 1) {
  labels <- test.phy$tip.label
  nsp <- length(labels)
  if (n > nsp) { stop("can't have more classes than tips in the tree") }
  if (n < 1) { stop("must have at least one class") }
  if (n == 1) { warning("exactly one class has a trivial solution") }
  # want at least min.num members of every class, then pick others randomly
  first.bit <- rep(1:n, min.num)
  if (nsp < length(first.bit)) { stop("can't guarantee min.num with this few tips") }
  rest <- sample(1:n, (nsp - length(first.bit)), replace = TRUE)
  # now shuffle to get rid of effect of first bit
  regimes <- sample(c(first.bit, rest))
  names(regimes) <- labels
  if (min(count.each(regimes)) < 2) { warning("some classes have only one member") }
  as.factor(regimes)
}

rnd.ou.multi.sim  <- function(test.phy, n, min.num = 2,
  param = list(sigma = 13.58, alpha = 1, theta = c(0, 0), ntraits = 1, root = FALSE)) {
  param$theta <- rep(0, n)
  regimes <- make.random.selective.regimes(test.phy, n, min.num)
  simmap <- make.simmap(test.phy, regimes, model = "ER", nsim = 1, message = FALSE)
  simtrait <- mvSIM(simmap, nsim = 1, model = "OUM", param = param)
  if (ncol(simtrait) == 1) simtrait[,1] else simtrait
}

# Try dynamically truncating!
test.power <- function(test.phy,
  effect.size = 0.5,
  alpha = 1,
  beta0 = 0,
  level = 0.05,
  B = 100,
  distro = FALSE,
  type = "phylo",
  contModel = "OU",
  contParams = list(alpha = 0,
    sigma2 = 13.58,
    ancestral.state = -4.67,
    optimal.value = 0),
  oumParams = NULL,
  oumN = 2,
  n.cores = 1,
  theta.mean = -4.67,
  theta.sd = 1.51,
  subsample = NULL, 
  use.existing.cont = NULL,
  left.truncate = NULL,
  dynamic.truncate = NULL,
  cl = NULL
) {
  force(contParams)
  force(oumParams)
  force(dynamic.truncate)
  force(left.truncate)
  force(use.existing.cont)
  nspp <- length(test.phy$tip.label)
  if (n.cores == 1) {
    applyfxn <- pbsapply
  } else {
    if (is.null(cl)) {
      cl <- makeCluster(n.cores)
      noStop = FALSE
    } else {
      noStop = TRUE
    }
    clusterCall(cl, source, file="plr-midas-functions.R")
    clusterCall(cl, source, file="plm-power-functions.R")
    applyfxn <- function(X, FUN, ...) {
      tryCatch(simplify2array(pblapply(X, FUN, ..., cl = cl)), 
        error = function(e) { warning(paste(e)); NA }
      )
    }
  }
  test.pv <- applyfxn(1:B, function(.) {
    # note, this is a different "alpha" and is supposed to be zero despite the
    # alpha in the parameters above... sigh
    if (is.null(use.existing.cont)) {
      if (contModel %in% c("OUM", "OUMT")) {
        capture.output({
          trait1 <- tryCatch({
            # For some reason every once in a while this fails
            if (is.null(oumParams)) {
              if (contModel == "OUMT") {
                warning("need to provide params explicitly with OUMT; generating with theta = 0")
              }
              trait1 <- rnd.ou.multi.sim(test.phy, oumN, min.num = 2)
            } else {
              if (contModel == "OUMT") { oumParams$theta <- rnorm(oumN, theta.mean, theta.sd)  }
              trait1 <- rnd.ou.multi.sim(test.phy, oumN, min.num = 2, param = oumParams)
            }
          }, error = function(e) { warning(paste0("OU simulation failed (",e,")")); NA })
          if (is.na(trait1)) return(NA)}, file = "ou-log.log", append = TRUE)
      } else {
        trait1 <- rTrait(n = 1, test.phy, model = contModel, parameters = contParams)
      }
    } else {
      trait1 <- use.existing.cont[intersect(names(use.existing.cont), test.phy$tip.label)]
    }
    if (!is.null(subsample)) {
      if (subsample < nspp) {
        trait1 <- sample(trait1, subsample)
        test.phy <- keep.tips(test.phy, names(trait1))
        nspp <- length(test.phy$tip.label)
      } else {
        warning("subsample number is larger than total number of species, using full tree")
      }
    }
    if (!is.null(left.truncate)) { 
      trait1[which(trait1 <= left.truncate)] <- left.truncate
    }
    if (var(trait1) == 0) { warning("no variance in continuous trait"); return(NA) }
    trait1.mc <- (function(x) (x - mean(x))/sd(x))(trait1)
    trait2 <- rbinTrait(n = 1,
      test.phy,
      beta = c(beta0, effect.size),
      alpha = alpha,
      X = cbind(rep(1, nspp),
        trait1.mc))
    if (!is.null(dynamic.truncate)) {
      trait1.mc[which(trait1.mc <= dynamic.truncate)] <- dynamic.truncate
      trait1 <- (trait1.mc * sd(trait1)) + mean(trait1)
    }
    if (var(trait2) == 0) { warning("no variance in binary trait"); return(NA) }
    if (type == "phylo") {
      tryCatch(
        summary(phylolm(trait1 ~ trait2,
            phy = test.phy))$coefficients[2, "p.value"],
        error = function(e) { warning(paste(e)); NA }
      )
    } else if (type %in% c("phylo.censored", "phylo.empirical")) {
      if (type == "phylo.empirical") { 
        actually.uncensor <- FALSE
      } else { actually.uncensor <- TRUE }
      tryCatch(cppb.wrapper(trait2,
          trait1,
          phy = test.phy,
          B = 500,
          strict = TRUE,
          maxfail = 5,
          na.threshold = 0.2,
          adaptive = TRUE,
          adapt.cut = 0.05,
          actually.uncensor = actually.uncensor,
          blocksize = 25)["sX", "p.value"],
        error = function(e) { warning(paste(e)); NA })
    } else if (type == "lm") {
      tryCatch(summary(lm(trait1 ~ trait2))$coefficients[2,"Pr(>|t|)"],
        error = function(e) NA)
    } else {
      error("type needs to be either 'phylo', 'phylo.censored', 'phylo.empirical', or 'lm'")
    }
  })
  if (n.cores > 1) {
   if (!noStop) { stopCluster(cl) }
  }
  if (distro) return(test.pv)
  mean(na.omit(test.pv) <= level)
}


require(fitdistrplus)
require(truncnorm)

pseudoreads <- function(mtx, r = 50000) {
  round(renorm.ra(mtx) * r)
}

renorm.ra <- function(mtx) {
  apply(mtx, 2, function(x) x/sum(na.omit(x)))
}

drop.n.highest <- function(mtx, n = 25, renorm = TRUE, drop.by = "abundance") {
  if (drop.by == "abundance") {
    highest <- sort(rowSums(mtx), dec = T)[1:n] %>% names
  } else if (drop.by == "prevalence") {
    highest <- sort(rowSums(1 * (mtx > 0)), dec = T)[1:n] %>% names
  } else {
    stop("must choose abundance or prevalence")
  }
  dropped <- mtx[setdiff(rownames(mtx), highest), ]
  if (renorm) renorm.ra(dropped) else dropped
}

estprev <- function(mtx, pc0 = 1, pc1 = 1, threshold = 0) {
  apply(mtx, 1, function(x) mean(na.omit(c(rep(0, pc0), rep(1, pc1), x) > threshold)))
}

compare.dropped <- function(mtx, n = 25, r = 50000) {
  truth = estprev(mtx)
  downsampled1 = pseudoreads(drop.n.highest(mtx, n), r) %>% estprev
  downsampled2 = pseudoreads(mtx, r) %>% estprev
  small.merge(data.frame(truth=truth), small.merge(data.frame(downsampled1=downsampled1), data.frame(downsampled2=downsampled2)))
}

fake.uncensor <- function(distro, B = 100, return.fit = FALSE) {
  cval <- min(distro)
  d2 <- cbind(left = distro, right = distro)
  d2[which(d2[, 1] == cval), 1] <- NA
  dfit <- fitdistcens(data.frame(d2), "norm")
  m <- dfit$estimate["mean"]
  s <- dfit$estimate["sd"]
  cens <- which(distro == cval)
  at.which <- (cval - m) / s
  r <- sapply(1:B, function(.) {
    d3 <- distro
    d3[cens] <- rtruncnorm(n = length(cens), a = -Inf, b = cval, mean = m, sd = s)
    d3
  })
  if (return.fit) return(list(results = r, fit = dfit, distro = distro, cens = cens, at.which = at.which))
  r
}

plm.coefs <- function(sX, sY, phy) {
  p <- keep.tips(phy, names(sY))
  o <- tryCatch(
    phylolm(sY ~ sX, phy = p) %>% coefficients, 
    error = function(e) { warning(paste(e)); c(NA, NA) })
  o
}

lm.coefs <- function(sX, sY) {
  o <- tryCatch(
    lm(sY ~ sX) %>% coefficients, 
    error = function(e) { warning(paste(e)); c(NA, NA) })
  o
}

censored.phylolm.parametric.bs <- function(X,
                                           Y,
                                           phy,
                                           use.phy = TRUE,
                                           B = 100,
                                           strict = FALSE,
                                           maxfail = 5,
                                           adaptive = FALSE,
                                           adapt.cut = 0.15,
                                           blocksize = 20,
                                           actually.uncensor = TRUE,
                                           actually.simulate = TRUE
) {
  # NB: Adaptive does early stopping after adapt.cut * B rejections of the null
  # Should be helpful for not wasting cycles to get high p-values super
  # accurately
  if (actually.uncensor)
  {
    uY <- fake.uncensor(Y, B)
  } else { 
    uY <- matrix(rep(Y, B), nc = B)
    rownames(uY) <- names(Y)
  }
  pglm <- phyloglm(phy = phy, X ~ 1) 
  n.reject <- 0
  adapt.cut.n <- ceiling(adapt.cut * B)
  break.out <- FALSE
  if (!adaptive) { blocksize <- B }
  result.mtx <- matrix(nr = 4, nc = 0, dimnames = list(c("null.intercept", "null.sX", "intercept", "sX")))
  i <- 0
  while (!break.out) {
    block.result <- sapply(1:blocksize, function(b) {
      ib <- i + b
      redo <- TRUE
      fail <- 0
      while (redo) {
        sY <- uY[, b]
        if (actually.simulate) {
          for (index in 1:10) {
            sX <- rbinTrait(n = 1, phy, pglm$coefficients[1], pglm$alpha)
            if (sum(sX) > 0) { break }
          }
        } else {
          sX <- sample(X)
        }
        if (use.phy) {
          p <- keep.tips(phy, names(sY))
          o1 <- plm.coefs(sX, sY, p)
          o2 <- plm.coefs(X, sY, p)
          o <- c(o1, o2)
        } else {
          o1 <- lm.coefs(sX, sY)
          o2 <- lm.coefs(X, sY)
          o <- c(o1, o2)
        }
        if ((is.na(o[1]) | is.na(o[3])) & strict & (fail < maxfail)) {
          redo <- TRUE
          fail <- fail + 1
        } else { redo <- FALSE }
      }
      o
    })
    i <- i + blocksize
    est.x <- mean(na.omit(block.result[4, ]))
    est.null.x <- mean(na.omit(block.result[2, ]))
    null.ctr <- (block.result[2, ] - est.null.x)
    x.ctr <- est.x - est.null.x
    n.extreme <- sum((null.ctr ** 2) >= x.ctr)
    result.mtx <- cbind(result.mtx, block.result)
    n.reject <- n.reject + n.extreme
    if (ncol(result.mtx) >= B) { break.out <- TRUE }
    if (n.reject >= adapt.cut.n) { break.out <- TRUE }
  }
  result.mtx
}

parametric.pv <- function(cppb.output, na.threshold = 0.5) {
  sapply(1:(nrow(cppb.output)/2), function(x) {
    n <- x
    r <- x + 2
    if (mean(is.na(cppb.output[n, ])) > na.threshold) { return(NA) }
    if (mean(is.na(cppb.output[r, ])) > na.threshold) { return(NA) }
    null.avg <- mean(na.omit(cppb.output[n, ]))
    real.avg <- mean(na.omit(cppb.output[r, ]))
    null.dist <- na.omit(cppb.output[n, ]) - null.avg
    mean(c(TRUE, (null.dist ** 2) >= ((real.avg - null.avg) ** 2)))
  })
}

parametric.coefs <- function(cppb.output, na.threshold = 0.5) {
  cbind(Estimate=apply(cppb.output[3:4, ], 1, function(x) mean(x, na.rm = T)),
        p.value=parametric.pv(cppb.output, na.threshold))
}

cppb.wrapper <- function(X, Y, phy = NULL, na.threshold = 0.5, ...) {
  censored.phylolm.parametric.bs(X, Y, phy, ...) %>% parametric.coefs(., na.threshold)
}


censored.phylolm.subsample <- function(X,
                                       Y,
                                       phy = NULL,
                                       B = 100,
                                       pct = 0.75,
                                       strict = FALSE,
                                       maxfail = 5,
                                       adaptive = FALSE,
                                       adapt.cut = 0.15,
                                       blocksize = 20,
                                       actually.uncensor = TRUE,
                                       jackknife = TRUE
) {
  # NB: Adaptive does early stopping after adapt.cut * B rejections of the null
  # Should be helpful for not wasting cycles to get high p-values super
  # accurately
  if (actually.uncensor)
  {
    uY <- fake.uncensor(Y, B)
  } else { 
    uY <- matrix(rep(Y, B), nc = B)
    rownames(uY) <- names(Y)
  }
  n.reject <- 0
  adapt.cut.n <- ceiling(adapt.cut * B)
  break.out <- FALSE
  if (jackknife) { strict <- FALSE }
  if (!adaptive) { blocksize <- B }
  result.mtx <- matrix(nr = 2, nc = 0, dimnames = list(c("(Intercept)", "sX")))
  if (jackknife) { j <- sample(1:nrow(uY), B, replace = FALSE); i <- 0 }
  while (!break.out) {
    block.result <- sapply(1:blocksize, function(b) {
      ib <- i + b
      redo <- TRUE
      fail <- 0
      while (redo) {
        if (!jackknife) {
          s <- sample(1:length(Y), round(length(Y) * pct), replace = FALSE)
        } else {
          s <- setdiff(1:length(Y), j[ib])
        }
        sY <- uY[s, b]
        sX <- X[s]
        if (!is.null(phy)) {
          p <- keep.tips(phy, names(sY))
          o <- tryCatch(
            phylolm(sY ~ sX, phy = p) %>% coefficients, 
            error = function(e) { warning(paste(e)); c(NA, NA) })
          o
        } else {
          o <- tryCatch(
            lm(sY ~ sX, phy = p) %>% coefficients,
            error = function(e) { warning(paste(e)); c(NA, NA) })
        }
        if (is.na(o[1]) & strict & (fail < maxfail)) {
          redo <- TRUE
          fail <- fail + 1
        } else { redo <- FALSE }
      }
      o
    })
    i <- i + blocksize
    n.extreme <- pvalify(block.result, sum = TRUE, jackknife = jackknife)["sX"]
    result.mtx <- cbind(result.mtx, block.result)
    n.reject <- n.reject + n.extreme
    if (ncol(result.mtx) >= B) { break.out <- TRUE }
    if (n.reject >= adapt.cut.n) { break.out <- TRUE }
  }
  result.mtx
}

pvalify <- function(cps.output, na.threshold = 0.2, sum = FALSE, jackknife = TRUE, graph = FALSE) {
  apply(cps.output, 1, function(x) {
    mna <- mean(is.na(x))
    if (mna > na.threshold) {
      warning(paste("na.threshold exceeded:", mna))
      return(NA)
    }
    nx <- na.omit(x)
    null.dist <- nx - mean(nx)
    # n-1 correction for jackknife distribution
    if (jackknife) {
      jk.var <- (length(null.dist) - 1) * (sum(null.dist ** 2) / length(null.dist))
      null.dist <- (null.dist / sd(null.dist)) * sqrt(jk.var)
    }
    null.magnitude <- null.dist ** 2
    est.magnitude <- mean(nx) ** 2
    if (graph) {
      plot(density(null.magnitude, from = 0, to = max(c(null.magnitude, est.magnitude))))
      abline(v = est.magnitude)
    }
    # n.tot <- (c(((nx - mean(nx)) ** 2), mean(nx) ** 2) >= (mean(nx) ** 2))
    tf <- c(est.magnitude, null.magnitude) >= est.magnitude
    if (sum) return(sum(tf)) else return(mean(tf))
  })
}

cps.coefs <- function(cps.output, na.threshold = 0.2, jackknife = TRUE, ...) {
  cbind(Estimate=apply(cps.output, 1, function(x) mean(x, na.rm = T)),
        p.value=pvalify(cps.output, na.threshold, jackknife = jackknife, ...))
}

cps.wrapper <- function(X, Y, phy = NULL, B = 100, pct = 0.75, strict = TRUE, maxfail = 5, na.threshold = 0.2, jackknife = TRUE, ...) {
  censored.phylolm.subsample(X, Y, phy, B, pct, strict, maxfail, jackknife = jackknife, ...) %>% cps.coefs(., na.threshold, jackknife = jackknife)
}

pr.wrapper <- function(X, Y, phy = NULL, B = 100, pct = 0.75, strict = TRUE, maxfail = 5, na.threshold = 0.2, jackknife = TRUE, ...) {
  censored.phylolm.subsample(X, Y, phy, B, pct, strict, maxfail, actually.uncensor = FALSE, jackknife = jackknife, ...) %>% cps.coefs(., na.threshold, jackknife = jackknife)
}

# need to find resampling percentage that minimizes KS distance from jackknife
# but think this might be wrong actually...
find.right.percentage <- function(X, Y, phy, q = 0.9, maxk = 30, B = 100) {
  common <- Reduce(intersect, list(phy$tip.label, names(X), names(Y)))
  Xc <- X[common]
  Yc <- Y[common]
  phyc <- keep.tips(phy, common)
  n <- length(common)
  b <- sapply(0:maxk, function(k) ceiling(n*(q**k)))
  e.distros <- pblapply(b, function(b.k) {
    if (b.k == n) { b.k <- (n - 1) } # jackknife estimate
    sapply(1:B, function(.) {
      s <- sample(common, b.k, replace = FALSE)
      sX <- Xc[s]
      sY <- Yc[s]
      p <- keep.tips(phy, names(sY))
      o <- tryCatch(
            phylolm(sY ~ sX, phy = p) %>% coefficients, 
            error = function(e) { warning(paste(e)); c(NA, NA) })
      o[2]
    })
  })
  ks.dists <- sapply(e.distros, function(ed) {
    e <- na.omit(ed)
    if ((length(e) / length(ed)) <= 0.5) {
      1
    }  else {
      ks.test(ed, e.distros[[1]])$statistic
    }
  })
  # print(ks.dists)
  best.k <- (which(ks.dists[-1] == min(ks.dists[-1]))) + 1
  # print(best.k)
  # print(q ** best.k)
  best.bk <- max(b[best.k])
  best.bk
}
