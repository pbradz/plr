# README: PLR #

Rmarkdown notebook for "Phylogeny-corrected identification of microbial gene families relevant to human gut colonization" (submitted).

Version of the codebase associated with the resubmitted manuscript is v0.99.

Draft manuscript: BIORXIV-2017-189795v1-Pollard-1.pdf
.Rmd notebook: plm-analyses.Rmd
  
Dependencies:

  * CRAN
    - data.table
    - phylolm
    - rentrez
    - pbapply
    - XML
    - magrittr
    - phytools
    - gridExtra
    - igraph
    - MASS
    - nlme
    - ape
    - geiger
    - mvMORPH
    - openxlsx
  * BioConductor
    - qvalue
    - ggtree
    - phyloseq


To compile the notebook:

  * Check out the repository using "git clone."
  * If you haven't installed R already, download and install [RStudio](https://www.rstudio.com/). (Instructions will be for RStudio, but it is also possible to compile the notebook without the use of RStudio by using [knitr](https://yihui.name/knitr/) directly.)
  * Download data files from Figshare: [https://figshare.com/articles/Phylogenetic_regression_input_and_intermediate_files/6007697](https://figshare.com/articles/Phylogenetic_regression_input_and_intermediate_files/6007697)
    - Untar intermediates.tar in the same directory as the git repo. This provides intermediate data files to avoid having to regenerate the most time-consuming analyses from scratch.
    - Unzip and untar (using the option -J, or unzipping first using xz) the data archive input_data.tar.xz in the same directory as the git repo. This provides input data files.
    - Similarly, unzip and untar power-res.tar.xz in the same directory as the git repo to reproduce the censoring power analysis.
    - If you want to reproduce the *in vivo* enrichment analyses, download [this supplemental file from Wu et al., 2015](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4608238/bin/NIHMS728455-supplement-Supplemental.xlsx) and place it in "data/screens/" under the main git repo directory.
    - If you want to re-analyze the MIDAS database to get matrices of FIGfam presence/absence by species, you will additionally need to download the [MIDAS v1.0 database](http://lighthouse.ucsf.edu/MIDAS/midas_db_v1.0.tar.gz) and unzip/untar it under "data/midas/".
  * Open install-dependencies.R in RStudio and run it. RStudio may restart partway through running this script, so you may have to run it more than once until you get the message "Complete".
  * Open plm-analyses.Rmd in RStudio and click "knit".

