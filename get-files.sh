#!/bin/sh

wget https://ndownloader.figshare.com/articles/6007697/versions/1 -O 6007697.zip
unzip 6007697.zip
tar -xvf intermediates.tar
tar -Jxvf power-res.tar.xz
tar -Jxvf input_data.tar.xz

wget https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4608238/bin/NIHMS728455-supplement-Supplemental.xlsx
mkdir data/screens
mv NIHMS728455-supplement-Supplemental.xlsx ./data/screens

# Uncomment to grab MIDAS database (large, slow, only necessary if you want to process it yourself)
# mkdir data/midas
# cd data/midas
# wget http://lighthouse.ucsf.edu/MIDAS/midas_db_v1.0.tar.gz
# tar xvfz midas_db_v1.0.tar.gz
# cd ../..

